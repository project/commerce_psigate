INTRODUCTION
------------

Integrates the PSiGate Real-time XML API with Drupal Commerce for on-site
payments.

 * More info on PSiGate - http://www.psigate.com/

 * Documentation for PSiGate XML API can be downloaded here:
   http://www.psigate.com/wp-content/uploads/2015/10/XML-Messenger-Kit.zip

 * This module supports PreAuth and Sale transaction types. Other
   transaction types such as PostAuth, Credit and Void can be completed
   via the PSiGate Merchant Tools.


REQUIREMENTS
------------

This module requires the following modules:

 * Commerce (https://www.drupal.org/project/commerce)
 * Commerce UI
 * Payment
 * Order


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7 for
   further information.

 * Enable the payment method rule titled PSiGate via
   Store > Configuration > Payment methods and edit its action to enter your
   PSiGate credentials.

 * You must also choose whether you want to perform Authorization and capture
  (Sale) or Authorization only (PreAuth) transactions during checkout.
  Authorization only transactions can be captured by logging into the PSiGate
  Merchant Tools.

 * The Payment checkout pane must come after the Billing information checkout
   pane for Drupal Commerce to be able to populate the order used to generate
   transactions with a billing and shipping address before this module attempts
   to process payment.


TEST TRANSACTIONS
-----------------

PSiGate provides a shared test gateway. Do NOT use real credit card numbers
within the test environment.

To enable the test gateway, navigate to Store > Configuration > Payment
methods and edit the PSiGate action to bring up the PSiGate payment
settings page and enter:
 * StoreID: teststore
 * Passphrase: psigate1234
 * Select "Test transactions on a developer account" under Transaction mode.

To review your test transactions, log into the test PSiGate Merchant Tools
account at https://dev.psigate.com with the following account information:
 * CID: 1000001
 * User: teststore
 * Pass: testpass
